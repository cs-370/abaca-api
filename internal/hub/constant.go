package hub

//Constants used for Hub Communication

type RequestMajorByte byte

//Categories for Requests
const (
	//Initial Connection to Hub.
	Hello RequestMajorByte = iota
	//Query Servers available, or information about a server
	Query
	//Create a shared secret with another server
	Link
	//Send a message to another Server
	Forward
	//Stay connected, See if new messages are available
	Ping
)

type RequestMinorByte byte

const (
	//Used with most connections, other flags set are based on major byte.
	DefaultMinor RequestMinorByte = iota
	// Query server data
	QueryServers
	// Get the public key of another server
	QueryServerInfo
	// Server Replying from a forward
	ForwardReply
	//Request to Encrypt, may not be supported.
	KeyExchange

	//Request to share a chatroom with another server
	ShareChatroom

	//Send a message to a foreign server
	SendChatMsg

	// Request to share a message with another server
	ListMessagesRequest

	// Server response with messages from id
	ListMessagesResponse
)
