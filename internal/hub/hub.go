package hub

import (
	"fmt"
	"net"
	"sync"
	"time"
)

type Hub struct {
	//Maps UUID -> Server
	servers map[string]Server
	rwMutex *sync.RWMutex
}

//TCP Server for sending Abaca messages
func (hub Hub) Serve() error {
	//Initialize hub variables
	hub.rwMutex = &sync.RWMutex{}
	hub.servers = make(map[string]Server)

	// Start a TCP Socket listener
	link, err := net.Listen("tcp", ":12131")
	if err != nil {
		return err
	}
	//TCP Listen loop
	for {
		conn, err := link.Accept()
		if err != nil {
			fmt.Println(time.Now(), ":", err)
		}
		//Runs each connection concurrently
		go hub.handleConnection(conn)
	}

}

//Controls the communication over the TCP link
func (hub Hub) handleConnection(conn net.Conn) {
	var message []byte
	for {
		//Max frame size
		buffer := make([]byte, 1500)
		count, err := conn.Read(buffer)
		if err != nil {
			fmt.Println(err)
			goto exit
		}
		fmt.Println("Read ", count, " bytes from ", conn.RemoteAddr())
		//Collect the message
		message = append(message, buffer...)
		//Try to parse the message
		switch resp := hub.parseMessage(message, conn).(type) {
		// True means more data is needed, wait for it.
		// False means a critical error occurred, so drop the connection
		case bool:
			//Give up and close the connection.
			if !resp {
				goto exit
			}
		//Send data back to the client
		case []byte:
			//Send a reply
			_, _ = conn.Write(resp)
			message = nil
		//Ignore the message, but keep listening for more messages
		default:
			message = nil
		}
	}
exit:
	if len(message) > 0 && RequestMajorByte(message[0]) != Hello {
		conn.Close()
	}

}
