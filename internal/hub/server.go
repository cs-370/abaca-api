package hub

import (
	"crypto/rsa"
	"encoding/binary"
	"fmt"
	"github.com/google/uuid"
	"net"
	"sync"
	"time"
)

//API Server connected to this hub.
type Server struct {
	lock *sync.Mutex
	//UUID of the server
	id uuid.UUID
	//Server's Public Key
	serverKey *rsa.PublicKey
	//Messages being sent to this server
	InboundBuffer chan []byte
	//Messages being received by this server.
	OutboundBuffer chan []byte

	//Continuous stream to send data to the server
	conn net.Conn
}

//Transforms a network connection into a forwarding connection, which runs forever and handles i/o
//Locks the connection
func (srv *Server) Forward() {
	var msg []byte
	fmt.Println("Set up stream from ", srv.conn.RemoteAddr(), " to hub.")
	for {
		//Receive a message to forward
		msg = <-srv.InboundBuffer
		_, err := srv.conn.Write(msg)
		if err != nil {
			return
		}
		//Read response
		reply := srv.readReply()
		select {
		//Send response back
		case srv.OutboundBuffer <- reply:
		//Should the outbound buffer listener give up, avoid hanging forever
		case <-time.After(time.Minute):
		}

	}
}

//Reads a reply from the server
// Format:
//	2 byte header
//  36 byte sender id
//	36 byte receiver id
//	8 byte big endian uint64 content length
//  X bytes, as specified by content length
//  2 byte footer
//	Return value:
//		Server response or error
func (srv *Server) readReply() []byte {
	buffer := make([]byte, 82)
	var message []byte
	//Read the header
	_, err := srv.conn.Read(buffer)
	if err != nil {
		return nil
	}
	if buffer[0] != byte(Forward) || buffer[1] != byte(ForwardReply) {
		return nil
	}
	contentLength := binary.BigEndian.Uint64(buffer[74:82])

	message = buffer
	buffer = make([]byte, contentLength+2)
	_, err = srv.conn.Read(buffer)
	//footer := buffer[contentLength:]
	//if len(footer) < 2 || footer[0] != 0xAB || footer[1] != 0xAC {
	//	return nil
	//}
	return append(message, buffer...)
}
