package hub

import (
	"crypto/x509"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"net"
	"sync"
	"time"
)

//Read messages over TCP
//Returns a boolean if the message state is unreadable
//	- true = keep waiting for more content
//  - false = give up, discard the message, and close the connection
//Returns a []byte if a reply is necessary
func (hub Hub) parseMessage(message []byte, conn net.Conn) (response interface{}) {
	if len(message) < 2 {
		return true
	}
	switch RequestMajorByte(message[0]) {
	//Introductory Message Telling the Hub a new server wants to join
	case Hello:
		//Do not close on hello connections
		if RequestMinorByte(message[1]) == DefaultMinor {
			return hub.parseHello(message, conn)
		}
	case Query:
		return hub.parseQuery(message, conn)
	case Forward:
		return hub.parseForward(message, conn)
	default:
	}
	return nil
}

//Respond to a hello message from a new client.
//The hello message has the following format
// 2 byte standard header
// 36 bytes for the client's UUID
// 270 bytes for the client's public key
// 2 byte footer 0xAB 0xAC
//Return format is the same as parseMessage
func (hub *Hub) parseHello(msg []byte, conn net.Conn) interface{} {

	if len(msg) < 310 {
		return true
	}
	//Check footer
	if msg[308] != 0xAB || msg[309] != 0xAC {
		return false
	}
	id := string(msg[2:38])
	rawKey := []byte(msg[38:308])
	key, err := x509.ParsePKCS1PublicKey(rawKey)
	if err != nil {
		return []byte("Invalid Public Key")
	}
	serverID, err := uuid.Parse(id)
	if err != nil {
		return []byte("Invalid Server ID")
	}
	//Add the server to the list of servers attached to the hub.
	hub.rwMutex.Lock()

	//Disconnect last session
	if srv, ok := hub.servers[id]; ok && srv.conn != nil {
		srv.conn.Close()

	}
	hub.servers[id] = Server{
		lock:      &sync.Mutex{},
		id:        serverID,
		serverKey: key,
		//Each buffer can hold up to two messages before blocking any more.
		InboundBuffer:  make(chan []byte, 2),
		OutboundBuffer: make(chan []byte, 2),
		conn:           conn,
	}
	hub.rwMutex.Unlock()
	_, _ = conn.Write([]byte(id))
	//server.Forward() // Loop forever, handling connections
	//Keep the connection open
	for {
		time.Sleep(time.Hour)
	}

}

//Respond to a server query.
//Returns a list of servers connected to the hub.
//The Query message as the following format:
// 2 byte standard header
// Body Depends on MinorByte.
// 2 byte footer
func (hub *Hub) parseQuery(msg []byte, conn net.Conn) interface{} {

	switch RequestMinorByte(msg[1]) {
	//Request that Queries for servers connected to this hub.
	//No body is needed for this request.
	//Returns format:
	//	1 byte for number of uids
	//	list of ids separated by newlines and ending with newline 0
	case QueryServers:
		hub.rwMutex.RLock()
		defer hub.rwMutex.RUnlock()
		var resp []byte
		for id := range hub.servers {
			resp = append(resp, []byte(id)...)
			resp = append(resp, '\n')
		}
		//Add footer and byte with length of ids
		resp = append(resp, byte(0))
		resp = append([]byte{byte(len(hub.servers))}, resp...)
		return resp
	//Request that queries for the public key of a server connected to this id.
	// 36 byte body for server UUID
	// Return format:
	//   the public key
	//   newline
	case QueryServerInfo:
		if len(msg) < 40 {
			return false
		}
		if msg[38] != 0xAB || msg[39] != 0xAC {
			return false
		}
		serverID := msg[2:38]
		hub.rwMutex.RLock()
		defer hub.rwMutex.RUnlock()
		server, ok := hub.servers[string(serverID)]
		if !ok {
			return false
		}
		pubkey := server.serverKey
		raw, err := json.Marshal(pubkey)
		if err == nil {
			return append(raw, '\n')
		}

	}
	return false
}

//Forward Messages have the following format:
//	2 byte header
//  36 byte server ID of the sender
//  36 byte server ID of the receiver
//  8 byte big endian uint64 for the size of the message
//  X bytes specified above
//	2 byte footer
// The forwarded message has the same format as above
func (hub *Hub) parseForward(msg []byte, conn net.Conn) interface{} {
	if len(msg) < 84 {
		return true
	}
	senderID, err := uuid.ParseBytes(msg[2:38])
	if err != nil {
		return false
	}
	receiverID, err := uuid.ParseBytes(msg[38:74])
	if err != nil {
		return false
	}
	contentLength := binary.BigEndian.Uint64(msg[74:82])

	if contentLength+2 > uint64(len(msg[82:])) {
		return false
	}
	fmt.Println(senderID.String(), " sent ", receiverID.String(), " ", contentLength, " bytes")

	hub.rwMutex.Lock()
	server, ok := hub.servers[receiverID.String()]
	hub.rwMutex.Unlock()
	if !ok {
		return false
	}

	server.lock.Lock()
	defer server.lock.Unlock()
	if server.conn != nil {
		_, err := server.conn.Write(msg)
		if err != nil {
			return false
		}
		server.conn.SetReadDeadline(time.Now().Add(time.Second * 30))
		response := server.readReply()
		server.conn.SetReadDeadline(time.Time{})
		fmt.Println(receiverID.String(), " replied to ", senderID.String(), " with ", contentLength, " bytes")
		return response
	}
	return false
}
