package message

import (
	"encoding/binary"
	"encoding/json"
	"gitlab.com/cs-370/abaca-api.git/internal/hub"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/security"
)

//File for messages related to sharing chatrooms

type ShareChatroom struct {
}

func ShareChatroomRequest(senderID []byte, receiverID []byte, chatroomID int64, sessionKey []byte) ([]byte, error) {
	//Header
	header := []byte{byte(hub.Forward), byte(hub.ShareChatroom)}
	idBody, err := json.Marshal(chatroomID)
	if err != nil {
		return nil, err
	}
	cipher, err := security.Encrypt(sessionKey, idBody)
	if err != nil {
		return nil, err
	}
	//Content length
	contentLength := len(cipher)
	contentLengthByteString := make([]byte, 8)
	binary.BigEndian.PutUint64(contentLengthByteString, uint64(contentLength))
	//Assemble the pieces
	msg := append(header, senderID...)
	msg = append(msg, receiverID...)
	msg = append(msg, contentLengthByteString...)
	msg = append(msg, cipher...)
	msg = append(msg, 0xAB, 0xAC)

	return msg, nil

}
