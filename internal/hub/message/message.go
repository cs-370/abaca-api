package message

import (
	"crypto/rsa"
	"encoding/binary"
	"encoding/json"
	"gitlab.com/cs-370/abaca-api.git/internal/hub"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/security"
	"time"

	"github.com/google/uuid"
)

type KeyExchangeBody struct {
	CipherText []byte         `json:"cipher_text"`
	Key        *rsa.PublicKey `json:"key"`
}

type Message struct {
	Text     string    `json:"msg"`
	Username string    `json:"username"`
	SenderID uuid.UUID `json:"sender_id"`
	ChatID   int64     `json:"chat_id"`
	SendDate time.Time `json:"send_date"`
}

func KeyExchangeRequest(senderID []byte, receiverID []byte, ciphertext []byte, key *rsa.PublicKey) ([]byte, error) {
	//Header
	header := []byte{byte(hub.Forward), byte(hub.KeyExchange)}
	body := KeyExchangeBody{
		CipherText: ciphertext,
		Key:        key,
	}
	//Body
	rawBody, err := json.Marshal(&body)
	if err != nil {
		return nil, err
	}

	//Content length
	contentLength := len(rawBody)
	contentLengthByteString := make([]byte, 8)
	binary.BigEndian.PutUint64(contentLengthByteString, uint64(contentLength))
	//Assemble the pieces
	msg := append(header, senderID...)
	msg = append(msg, receiverID...)
	msg = append(msg, contentLengthByteString...)
	msg = append(msg, rawBody...)
	msg = append(msg, 0xAB, 0xAC)

	return msg, nil

}

func SendChatMessageRequest(senderID []byte, receiverID []byte, body Message, sessionKey []byte) ([]byte, error) {
	//Header
	header := []byte{byte(hub.Forward), byte(hub.SendChatMsg)}
	//Body
	rawBody, err := json.Marshal(&body)
	if err != nil {
		return nil, err
	}

	ciphertext, err := security.Encrypt(sessionKey, rawBody)
	if err != nil {
		return nil, err
	}

	//Content length
	contentLength := len(ciphertext)
	contentLengthByteString := make([]byte, 8)
	binary.BigEndian.PutUint64(contentLengthByteString, uint64(contentLength))
	//Assemble the pieces
	msg := append(header, senderID...)
	msg = append(msg, receiverID...)
	msg = append(msg, contentLengthByteString...)
	msg = append(msg, ciphertext...)
	msg = append(msg, 0xAB, 0xAC)

	return msg, nil

}

func ListMessagesRequest(senderID []byte, receiverID []byte, chatID int64, sessionKey []byte) ([]byte, error) {
	//Header
	header := []byte{byte(hub.Forward), byte(hub.ListMessagesRequest)}
	//Body

	rawBody, err := json.Marshal(&chatID)
	if err != nil {
		return nil, err
	}

	ciphertext, err := security.Encrypt(sessionKey, rawBody)
	if err != nil {
		return nil, err
	}

	//Content length
	contentLength := len(ciphertext)
	contentLengthByteString := make([]byte, 8)
	binary.BigEndian.PutUint64(contentLengthByteString, uint64(contentLength))
	//Assemble the pieces
	msg := append(header, senderID...)
	msg = append(msg, receiverID...)
	msg = append(msg, contentLengthByteString...)
	msg = append(msg, ciphertext...)
	msg = append(msg, 0xAB, 0xAC)

	return msg, nil

}
