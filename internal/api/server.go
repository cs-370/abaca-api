package api

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Server struct {
	r *mux.Router
}

func NewServer(r *mux.Router) Server {
	return Server{r: r}
}

func (s Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf(r.URL.Path)
	//CORS Headers
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Headers", "*")
	//Let the router handle the requests
	s.r.ServeHTTP(w, r)

}
