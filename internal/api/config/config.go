package config

import (
	"errors"

	"github.com/spf13/viper"
)

//Configuration for this API Server
var cfg = viper.New()

func Init() error {
	if cfg == nil {
		return errors.New("nil config")
	}

	cfg.SetConfigName("abaca")
	cfg.AddConfigPath(".")
	cfg.AddConfigPath("/etc/abaca")

	return cfg.ReadInConfig()
}
func Port() int {
	port := cfg.GetInt("abaca.port")
	if port == 0 {
		port = 3000
	}
	return port
}

/* DB Config */

func DBServer() string {
	host := cfg.GetString("db.host")
	if host == "" {
		host = "127.0.0.1"
	}
	return host
}
func DBName() string {
	name := cfg.GetString("db.name")
	if name == "" {
		name = "postgres"
	}
	return name
}

func DBUser() string {
	name := cfg.GetString("db.user")
	if name == "" {
		name = "postgres"
	}
	return name
}
func DBPassword() string {
	name := cfg.GetString("db.password")
	if name == "" {
		name = "postgres"
	}
	return name
}
func DBsslMode() string {
	name := cfg.GetString("db.sslmode")
	if name == "" {
		name = "disable"
	}
	return name
}
