package user

import (
	"net/http"

	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/creating"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/deleting"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/listing"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/updating"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers"
)

type Router struct {
	creator creating.Service
	lister  listing.Service
	updater updating.Service
	deleter deleting.Service
}

func NewUserRouter(creator creating.Service, lister listing.Service, updater updating.Service, deleter deleting.Service) Router {
	return Router{
		creator: creator,
		lister:  lister,
		updater: updater,
		deleter: deleter,
	}
}

func (user Router) GetRoutes() []routers.Route {
	return []routers.Route{
		{
			Path:    "/users",
			Handler: user.getAll,
			Methods: []string{
				http.MethodGet,
			},
		},
		{
			Path:    "/users",
			Handler: user.create,
			Methods: []string{
				http.MethodPost,
			},
		},
		{
			Path:    "/users/{id}",
			Handler: user.find,
			Methods: []string{
				http.MethodGet,
			},
		},
		{
			Path:    "/users/{id}",
			Handler: user.update,
			Methods: []string{
				http.MethodPut,
			},
		},
		{
			Path:    "/users/{id}",
			Handler: user.delete,
			Methods: []string{
				http.MethodDelete,
			},
		},
		{
			Path:    "/users/{id}/chatrooms",
			Handler: user.getChatrooms,
			Methods: []string{
				http.MethodGet,
			},
		},
	}
}
