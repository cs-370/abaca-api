package user

import (
	"encoding/json"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/knownerrors"
)

func (user Router) getAll(w http.ResponseWriter, r *http.Request) {
	users, err := user.lister.GetAllUsers()

	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	type UsersResponse struct {
		ID       uuid.UUID `json:"id"`
		Username string    `json:"username"`
	}
	var usersResponses []UsersResponse

	for _, user := range users {
		elem := UsersResponse{
			ID:       user.ID,
			Username: user.Username,
		}
		usersResponses = append(usersResponses, elem)
	}

	response := struct {
		Users []UsersResponse `json:"users"`
	}{Users: usersResponses}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)
}

func (user Router) create(w http.ResponseWriter, r *http.Request) {
	meta := struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{}

	if r.Body == nil {
		http.Error(w, knownerrors.UnauthorizedError, http.StatusBadRequest)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	id, err := user.creator.CreateUser(meta.Username, meta.Password)
	if err != nil {
		return
	}

	response := struct {
		ID uuid.UUID `json:"UUID"`
	}{ID: id}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)
}

func (user Router) find(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	uid, err := uuid.Parse(id)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	username, err := user.lister.GetUserByID(uid)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	response := struct {
		Username string `json:"username"`
	}{Username: username}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)

}

func (user Router) update(w http.ResponseWriter, r *http.Request) {
	meta := struct {
		Username string `json:"username"`
	}{}

	if r.Body == nil {
		http.Error(w, knownerrors.UnauthorizedError, http.StatusBadRequest)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)

	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	uid, err := uuid.Parse(id)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	err = user.updater.UpdateUser(uid, meta.Username)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

func (user Router) delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	uid, err := uuid.Parse(id)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	err = user.deleter.DeleteUser(uid)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

func (user Router) getChatrooms(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	uid, err := uuid.Parse(id)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	chatrooms, err := user.lister.GetAllChatroomsForUser(uid)

	type ChatroomsResponse struct {
		ID       int64     `json:"id"`
		Name     string    `json:"chatname"`
		ServerID uuid.UUID `json:"server_id"`
	}

	var chatroomsResponses []ChatroomsResponse

	if len(chatrooms) == 0 {
		response := struct {
			Chatrooms []ChatroomsResponse `json:"chatrooms"`
		}{Chatrooms: chatroomsResponses}

		raw, _ := json.Marshal(response)
		_, _ = w.Write(raw)
		return
	}

	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	for _, chatroom := range chatrooms {
		elem := ChatroomsResponse{
			ID:       chatroom.ChatID,
			Name:     chatroom.ChatName,
			ServerID: chatroom.ServerID,
		}
		chatroomsResponses = append(chatroomsResponses, elem)
	}

	response := struct {
		Chatrooms []ChatroomsResponse `json:"chatrooms"`
	}{Chatrooms: chatroomsResponses}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)
}
