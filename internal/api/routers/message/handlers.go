package message

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/knownerrors"
)

func (message Router) create(w http.ResponseWriter, r *http.Request) {
	meta := struct {
		Text     string    `json:"msg"`
		SenderID uuid.UUID `json:"sender_id"`
		ChatID   int64     `json:"chat_id"`
	}{}

	if r.Body == nil {
		http.Error(w, knownerrors.UnauthorizedError, http.StatusBadRequest)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	id, err := message.creator.CreateMessage(meta.SenderID, meta.Text, meta.ChatID)
	if err != nil {
		http.Error(w, knownerrors.InternalServerError, http.StatusInternalServerError)
		return
	}

	response := struct {
		ID int64 `json:"id"`
	}{ID: id}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)
}

func (message Router) find(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	intId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	m, err := message.lister.GetMessageByID(intId)

	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	response := struct {
		Text     string    `json:"msg"`
		SenderID uuid.UUID `json:"sender_id"`
		ChatID   int64     `json:"chat_id"`
		SendDate time.Time `json:"send_date"`
	}{
		Text:     m.Text,
		SenderID: m.SenderID,
		ChatID:   m.ChatID,
		SendDate: m.SendDate,
	}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)
}

func (message Router) update(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	intId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	meta := struct {
		Text string `json:"msg"`
	}{}

	if r.Body == nil {
		http.Error(w, knownerrors.UnauthorizedError, http.StatusBadRequest)
		return
	}
	err = json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	err = message.updater.UpdateMessage(intId, meta.Text)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

func (message Router) delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	intId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	err = message.deleter.DeleteMessage(intId)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}
