package message

import (
	"net/http"

	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/creating"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/deleting"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/listing"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/updating"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers"
)

type Router struct {
	creator creating.Service
	lister  listing.Service
	updater updating.Service
	deleter deleting.Service
}

func NewMessageRouter(creator creating.Service, lister listing.Service, updater updating.Service, deleter deleting.Service) Router {
	return Router{
		creator: creator,
		lister:  lister,
		updater: updater,
		deleter: deleter,
	}
}
func (message Router) GetRoutes() []routers.Route {
	return []routers.Route{
		{
			Path:    "/messages",
			Handler: message.create,
			Methods: []string{
				http.MethodPost,
			},
		},
		{
			Path:    "/messages/{id}",
			Handler: message.find,
			Methods: []string{
				http.MethodGet,
			},
		},
		{
			Path:    "/messages/{id}",
			Handler: message.update,
			Methods: []string{
				http.MethodPut,
			},
		},
		{
			Path:    "/messages/{id}",
			Handler: message.delete,
			Methods: []string{
				http.MethodDelete,
			},
		},
	}
}
