//Routers holds categorical route subpackages for the API.
//Each subpackage should represent the first part of the endpoint.
//(e.g. /msg/search/{id}  should be in package msg)
//The flow through the application is API -> Service -> Repository.
package routers

import (
	"net/http"

	"github.com/gorilla/mux"
)

//API Endpoints
type Route struct {
	//Endpoint for the struct
	Path string
	//Function called at the endpoint
	Handler http.HandlerFunc
	Methods []string
}

//Interface for API endpoint routers.
type EndpointSet interface {
	//Retrieves all of the endpoints from the router
	GetRoutes() []Route
}

//Wrapper to resolve CORS requests in Methods without the OPTIONS specifically specified.
func generateCORSHandler(route Route) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodOptions {
			for _, method := range route.Methods {
				if method == http.MethodOptions {
					route.Handler(w, r)
					return
				}
			}
			w.WriteHeader(http.StatusOK)
			return
		} else {
			//Handle request normally if not an OPTIONS request
			route.Handler(w, r)
		}

	}
}

func MapEndpointsToServer(routers []EndpointSet, serverRouter *mux.Router) {
	for _, router := range routers {
		for _, route := range router.GetRoutes() {
			serverRouter.Handle(route.Path, generateCORSHandler(route)).Methods(append(route.Methods, http.MethodOptions)...)
		}
	}
}
