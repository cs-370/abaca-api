package hub

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/knownerrors"
	"net/http"
	"strconv"
)

func (hub Router) RegisterHub(w http.ResponseWriter, r *http.Request) {
	meta := struct {
		Name    string `json:"name"`
		Address string `json:"address"`
	}{}

	err := json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}
	meta.Address += ":12131"

	id, err := hub.linker.BindToHub(meta.Address, meta.Name)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	raw, err := json.Marshal(struct {
		ID int64 `json:"id"`
	}{id})
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(raw)

}

func (hub Router) FindServersForHub(w http.ResponseWriter, r *http.Request) {

	hubName := r.URL.Query().Get("hub")

	servers, err := hub.sync.FindServersForHub(hubName)
	if err != nil {
		http.Error(w, knownerrors.BadGatewayError, http.StatusBadGateway)
		return
	}
	raw, err := json.Marshal(&servers)
	if err != nil {
		http.Error(w, knownerrors.InternalServerError, http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(raw)
}

func (hub Router) RegisterAPI(w http.ResponseWriter, r *http.Request) {
	meta := struct {
		Hub      string `json:"hub"`
		ServerID string `json:"server_id"`
	}{}
	err := json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}
	serverID, err := uuid.Parse(meta.ServerID)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}
	err = hub.linker.RegisterAPI(meta.Hub, serverID)
	if err != nil {
		http.Error(w, knownerrors.InternalServerError, http.StatusInternalServerError)
		return
	}
}

func (hub Router) ShareChatroom(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}
	meta := struct {
		Hub      string `json:"hub"`
		ServerID string `json:"server_id"`
	}{}
	err = json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}
	serverID, err := uuid.Parse(meta.ServerID)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	err = hub.linker.ShareChatroom(meta.Hub, serverID, int64(id))
	if err != nil {
		http.Error(w, knownerrors.InternalServerError, http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
