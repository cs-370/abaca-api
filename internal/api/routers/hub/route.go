package hub

import (
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/syncing"
	"net/http"

	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/linking"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers"
)

type Router struct {
	linker linking.Service
	sync   syncing.Service
}

func NewHubRouter(conn linking.Service, service syncing.Service) Router {
	return Router{
		linker: conn,
		sync:   service,
	}
}

func (hub Router) GetRoutes() []routers.Route {
	return []routers.Route{
		{
			Path:    "/hub/register",
			Handler: hub.RegisterHub,
			Methods: []string{
				http.MethodPost,
			},
		},
		{
			Path:    "/hub/servers",
			Handler: hub.FindServersForHub,
			Methods: []string{http.MethodGet},
		},
		{
			Path:    "/hub/link",
			Handler: hub.RegisterAPI,
			Methods: []string{
				http.MethodPost,
			},
		},
		{
			Path:    "/hub/share/chatroom/{id}",
			Handler: hub.ShareChatroom,
			Methods: []string{
				http.MethodPost,
			},
		},
	}
}
