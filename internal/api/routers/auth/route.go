package auth

import (
	"net/http"

	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/linking"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/listing"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers"
)

type Router struct {
	linker linking.Service
	lister listing.Service
}

func NewAuthRouter(conn linking.Service, lister listing.Service) Router {
	return Router{
		linker: conn,
		lister: lister,
	}
}

func (auth Router) GetRoutes() []routers.Route {
	return []routers.Route{
		{
			Path:    "/auth/login",
			Handler: auth.Login,
			Methods: []string{
				http.MethodPost,
			},
		},
		{
			Path:    "/auth/verify",
			Handler: auth.ValidateUser,
			Methods: []string{
				http.MethodGet,
			},
		},
	}
}
