package auth

import (
	"encoding/json"
	"net/http"

	"gitlab.com/cs-370/abaca-api.git/internal/pkg/knownerrors"
)

func (auth Router) Login(w http.ResponseWriter, r *http.Request) {

	meta := struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{}

	if r.Body == nil {
		http.Error(w, knownerrors.UnauthorizedError, http.StatusBadRequest)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	session, err := auth.linker.Login(meta.Username, meta.Password, "")
	if err != nil {
		http.Error(w, knownerrors.UnauthorizedError, http.StatusUnauthorized)
		return
	}

	response := struct {
		SessionToken string `json:"session"`
	}{SessionToken: string(session)}

	w.WriteHeader(200)
	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)

}

func (auth Router) ValidateUser(w http.ResponseWriter, r *http.Request) {
	if cookie, err := r.Cookie("session"); err == nil && cookie != nil {
		userID, err := auth.lister.GetUserIDForSession(string(cookie.Value))
		if err == nil {
			meta := struct {
				UserID string `json:"userID"`
			}{UserID: userID}
			data, err := json.Marshal(&meta)
			if err == nil {
				w.WriteHeader(http.StatusOK)
				_, _ = w.Write(data)
				return
			}
		}

	}
	w.WriteHeader(http.StatusUnauthorized)
}
