package auth

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/linking"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/listing"
)

type mockLinker struct {
	linking.Service
}

func (m mockLinker) Login(username string, password string, originServer string) (sessionToken []byte, err error) {
	if username == "u" && password == "p" {
		return []byte("sessionTokenString"), nil
	}
	return nil, errors.New("test error")
}

func Test_RouterLogin(t *testing.T) {
	cases := []struct {
		name         string
		body         io.Reader
		responseCode int
		responseBody string
	}{
		{
			name:         "HappyPath",
			body:         bytes.NewBufferString(`{"username":"u","password":"p"}`),
			responseCode: http.StatusOK,
		},
		{
			name:         "NoBody",
			body:         nil,
			responseCode: http.StatusBadRequest,
		},
		{
			name:         "BadBody",
			body:         bytes.NewBuffer(nil),
			responseCode: http.StatusBadRequest,
		},
		{
			name:         "InvalidPassword",
			body:         bytes.NewBufferString(`{"username":"u","password":"pa"}`),
			responseCode: http.StatusUnauthorized,
		},
	}

	for _, test := range cases {
		t.Run(test.name, func(t *testing.T) {
			router := NewAuthRouter(mockLinker{}, nil)
			w := httptest.NewRecorder()
			r, _ := http.NewRequest(http.MethodPost, "/", test.body)
			router.Login(w, r)
			assert.Equal(t, test.responseCode, w.Code)
		})
	}
}

type mockLister struct {
	listing.Service
	StringRet string
	ErrRet    error
}

func (m mockLister) GetUserIDForSession(sessionID string) (string, error) {
	return m.StringRet, m.ErrRet
}

func Test_RouterValidateUser(t *testing.T) {
	cases := []struct {
		name         string
		body         io.Reader
		cookie       string
		lister       listing.Service
		responseCode int
		responseBody string
	}{
		{
			name:         "HappyPath",
			body:         nil,
			cookie:       "session=0000",
			lister:       mockLister{},
			responseCode: http.StatusOK,
		},
		{
			name:         "UnauthorizedNoCookie",
			body:         nil,
			cookie:       "",
			lister:       mockLister{},
			responseCode: http.StatusUnauthorized,
		},
		{
			name:         "UnauthorizedInvalidCookie",
			body:         nil,
			cookie:       "session=0000",
			lister:       mockLister{ErrRet: errors.New("test error")},
			responseCode: http.StatusUnauthorized,
		},
	}

	for _, test := range cases {
		t.Run(test.name, func(t *testing.T) {
			//Arrange
			router := NewAuthRouter(mockLinker{}, nil)
			router.lister = test.lister
			//Create Web request
			w := httptest.NewRecorder()
			r, _ := http.NewRequest(http.MethodGet, "/", test.body)
			r.Header.Add("Cookie", test.cookie)
			//Act
			router.ValidateUser(w, r)
			assert.Equal(t, test.responseCode, w.Code)
		})
	}
}
