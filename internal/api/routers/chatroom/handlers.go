package chatroom

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/knownerrors"
)

func (chatroom Router) create(w http.ResponseWriter, r *http.Request) {
	meta := struct {
		Chatname string `json:"chatname"`
		ServerID int64  `json:"server_id"`
	}{}

	if r.Body == nil {
		http.Error(w, knownerrors.UnauthorizedError, http.StatusBadRequest)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	id, err := chatroom.creator.CreateChatRoom(meta.Chatname, meta.ServerID)
	if err != nil {
		return
	}

	response := struct {
		ID int64 `json:"id"`
	}{ID: id}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)
}

func (chatroom Router) find(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	intId, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	chat, err := chatroom.lister.GetChatroomByID(intId)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	response := struct {
		Chatname string    `json:"chatname"`
		ServerID uuid.UUID `json:"server_id"`
	}{
		Chatname: chat.ChatName,
		ServerID: chat.ServerID,
	}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)

}

func (chatroom Router) update(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}
	chatroomID, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	meta := struct {
		Chatname string `json:"chatname"`
		ServerID int64  `json:"server_id"`
	}{}

	if r.Body == nil {
		http.Error(w, knownerrors.UnauthorizedError, http.StatusBadRequest)
		return
	}

	err = json.NewDecoder(r.Body).Decode(&meta)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	err = chatroom.updater.UpdateChatroom(chatroomID, meta.Chatname, meta.ServerID)

	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

func (chatroom Router) delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}
	chatroomID, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	err = chatroom.deleter.DeleteChatroom(chatroomID)

	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}

func (chatroom Router) getMessages(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}
	chatroomID, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	messages, err := chatroom.lister.GetMessagesForChatroom(chatroomID)

	type MessageResponse struct {
		ID       int64     `json:"id"`
		Text     string    `json:"msg"`
		Username string    `json:"username"`
		SenderID uuid.UUID `json:"sender_id"`
		ChatID   int64     `json:"chat_id"`
		SendDate time.Time `json:"send_date"`
	}

	var messagesResponse []MessageResponse

	if len(messages) == 0 {
		response := struct {
			Messages []MessageResponse `json:"messages"`
		}{Messages: messagesResponse}

		raw, _ := json.Marshal(response)
		_, _ = w.Write(raw)
		return
	}

	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	for _, message := range messages {
		elem := MessageResponse{
			ID:       message.MessageID,
			Text:     message.Text,
			Username: message.Username,
			SenderID: message.SenderID,
			ChatID:   message.ChatID,
			SendDate: message.SendDate,
		}
		messagesResponse = append(messagesResponse, elem)
	}

	response := struct {
		Messages []MessageResponse `json:"messages"`
	}{Messages: messagesResponse}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)
}

func (chatroom Router) getMembers(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, ok := vars["id"]

	if ok == false {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}
	chatID, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	users, err := chatroom.lister.GetAllUSersForChatroom(chatID)

	type UsersResponse struct {
		ID       uuid.UUID `json:"id"`
		Username string    `json:"username"`
	}

	var usersResponses []UsersResponse

	if len(users) == 0 {
		response := struct {
			Users []UsersResponse `json:"users"`
		}{Users: usersResponses}

		raw, _ := json.Marshal(response)
		_, _ = w.Write(raw)
		return
	}

	if err != nil {
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	for _, user := range users {
		elem := UsersResponse{
			ID:       user.ID,
			Username: user.Username,
		}
		usersResponses = append(usersResponses, elem)
	}

	response := struct {
		Users []UsersResponse `json:"users"`
	}{Users: usersResponses}

	raw, _ := json.Marshal(response)
	_, _ = w.Write(raw)
}

func (chatroom Router) addMember(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	cID, ok1 := vars["id"]
	uID, ok2 := vars["userID"]

	if (ok1 == false) || (ok2 == false) {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	chatID, err := strconv.ParseInt(cID, 10, 64)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	userID, err := uuid.Parse(uID)
	if err != nil {
		http.Error(w, knownerrors.InvalidRequestError, http.StatusBadRequest)
		return
	}

	err = chatroom.creator.AddMemberToChatroom(chatID, userID)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
}
