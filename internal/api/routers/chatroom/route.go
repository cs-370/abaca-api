package chatroom

import (
	"net/http"

	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/creating"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/deleting"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/listing"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/updating"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers"
)

type Router struct {
	creator creating.Service
	lister  listing.Service
	updater updating.Service
	deleter deleting.Service
}

func NewChatroomRouter(creator creating.Service, lister listing.Service, updater updating.Service, deleter deleting.Service) Router {
	return Router{
		creator: creator,
		lister:  lister,
		updater: updater,
		deleter: deleter,
	}
}

func (chatroom Router) GetRoutes() []routers.Route {
	return []routers.Route{
		{
			Path:    "/chatrooms",
			Handler: chatroom.create,
			Methods: []string{
				http.MethodPost,
			},
		},
		{
			Path:    "/chatrooms/{id}",
			Handler: chatroom.find,
			Methods: []string{
				http.MethodGet,
			},
		},
		{
			Path:    "/chatrooms/{id}",
			Handler: chatroom.update,
			Methods: []string{
				http.MethodPut,
			},
		},
		{
			Path:    "/chatrooms/{id}",
			Handler: chatroom.delete,
			Methods: []string{
				http.MethodDelete,
			},
		},
		{
			Path:    "/chatrooms/{id}/messages",
			Handler: chatroom.getMessages,
			Methods: []string{
				http.MethodGet,
			},
		},
		{
			Path:    "/chatrooms/{id}/members",
			Handler: chatroom.getMembers,
			Methods: []string{
				http.MethodGet,
			},
		},
		{
			Path:    "/chatrooms/{id}/members/{userID}",
			Handler: chatroom.addMember,
			Methods: []string{
				http.MethodPost,
			},
		},
	}
}
