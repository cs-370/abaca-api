package syncing

import (
	"errors"
)

// Object representing other servers from hubs
type Server struct {
	//uuid for the server
	ID string `json:"id,omitempty"`
	//public key used to start sessions.
	//PubKey *rsa.PublicKey `json: "pubkey,omitempty"`

	//Session Key for servers with existing sessions
	//SessionKey []byte `json: "sessionKey,omitempty"`
}

//Enumerate a list of servers for a hub
func (s service) FindServersForHub(hubName string) ([]string, error) {
	hub, ok := s.hubs[hubName]
	if !ok {
		return nil, errors.New("hub not found")
	}
	return hub.FindServers()

}
