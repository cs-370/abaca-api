//Unlike other dal packages, this package is specifically for the hub.
//It acts as a bridge between the hub and other services
package syncing

import (
	"crypto/rsa"
	"errors"
	"gitlab.com/cs-370/abaca-api.git/internal/hub/message"
	"sync"
	"time"

	"github.com/google/uuid"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/storage/hubConnection"
)

type Service interface {
	//Adds a hub with the given name to the list of hubs
	RegisterNewHub(name string, addr string) error
	//Returns a list of server IDs for a given hub
	FindServersForHub(hubName string) ([]string, error)

	//Adds non existent hubs to the hub list
	UpdateHubList(hubs []hubConnection.Hub)

	//Links an API server to this one.
	LinkAPI(hubName string, serverID uuid.UUID) (sessionKey []byte, publicKey *rsa.PublicKey, err error)

	ShareChatroom(hubName string, remoteServer uuid.UUID, chatroomID int64) error
	SendMsgToForeignChat(senderID uuid.UUID, text string, chatRoomID int64) (int64, error)
	GetForeignMessages(chatID int64) ([]hubConnection.Message, error)
}

type service struct {
	//Database exclusively for hubs
	db               hubConnection.Repository
	hubs             map[string]hubConnection.HubConnection
	lock             *sync.RWMutex
	serverID         uuid.UUID
	serverPrivateKey *rsa.PrivateKey
}

//Creates a new service.
func NewService(key *rsa.PrivateKey, serverID uuid.UUID, db hubConnection.Repository) Service {
	return &service{
		serverPrivateKey: key,
		serverID:         serverID,
		hubs:             make(map[string]hubConnection.HubConnection),
		lock:             &sync.RWMutex{},
		db:               db,
	}
}

func (s *service) UpdateHubList(hubs []hubConnection.Hub) {
	for _, hub := range hubs {
		if _, ok := s.hubs[hub.Name]; !ok {
			s.hubs[hub.Name] = hubConnection.NewHubConnection(hub.Name, hub.Address, s.serverPrivateKey, s.db)
		}
	}
}

func (s service) RegisterNewHub(name string, addr string) error {
	newHub := hubConnection.NewHubConnection(name, addr, s.serverPrivateKey, s.db)
	err := newHub.Register(s.serverID, &s.serverPrivateKey.PublicKey)
	if err != nil {
		return err
	}
	go newHub.Sync(s.serverID, &s.serverPrivateKey.PublicKey)
	//Protect the new connection when it's added.
	s.lock.Lock()
	s.hubs[name] = newHub
	s.lock.Unlock()
	return nil
}

func (s service) LinkAPI(hubName string, serverID uuid.UUID) (sessionKey []byte, publicKey *rsa.PublicKey, err error) {
	s.lock.RLock()
	server, ok := s.hubs[hubName]
	s.lock.RUnlock()
	if !ok {
		return nil, nil, errors.New("no such hub exists")
	}
	remoteKey, err := server.GetPublicKey([]byte(serverID.String()))
	if err != nil {
		return nil, nil, err
	}

	sessionKey, err = server.RequestKeyExchange(serverID, remoteKey, s.serverID, s.serverPrivateKey)
	return sessionKey, remoteKey, err
}

func (s service) ShareChatroom(hubName string, remoteServer uuid.UUID, chatroomID int64) error {
	secret, err := s.db.GetServerSecret(remoteServer.String())
	if err != nil {
		return err
	}
	s.lock.RLock()
	hub := s.hubs[hubName]
	s.lock.RUnlock()
	return hub.ShareChatroom(s.serverID, remoteServer, chatroomID, secret)
}

//Converts a message from going into a normal chatroom to going into a foreign chatroom
//SenderID is the ID of the user sending this message
func (s service) SendMsgToForeignChat(msgSenderID uuid.UUID, text string, chatRoomID int64) (int64, error) {
	//Translate the normal chatroom into a foreign one
	chats, err := s.db.GetForeignChatRoomInfo(chatRoomID)
	if err != nil {
		return 0, err
	}
	var chatroom hubConnection.ForeignChat
	var hub hubConnection.HubConnection
	s.lock.RLock()
	//Verify connected hub exists
	for _, chat := range chats {
		if h, ok := s.hubs[chat.Name]; ok {
			chatroom = chat
			hub = h
			break
		}
	}
	if chatroom.ID == 0 {
		return 0, errors.New("could not find hub")
	}
	s.lock.RUnlock()

	//Get the secret to encrypt data for the server
	secret, err := s.db.GetServerSecret(chatroom.ServerID.String())
	if err != nil {
		return 0, err
	}
	//Get the username
	username, err := s.db.GetUserByID(msgSenderID)
	if err != nil {
		return 0, err
	}
	//Send the message
	return hub.SendMsg(s.serverID, chatroom.ServerID, message.Message{
		Text:     text,
		Username: username,
		SenderID: msgSenderID,
		ChatID:   chatroom.ID,
		SendDate: time.Now(),
	}, secret)

}

//Searches for a foreign chat's messages
func (s service) GetForeignMessages(chatID int64) ([]hubConnection.Message, error) {
	chats, err := s.db.GetForeignChatRoomInfo(chatID)
	if err != nil {

	}
	var hub *hubConnection.HubConnection
	var chatroom hubConnection.ForeignChat
	s.lock.RLock()
	for _, chat := range chats {
		if h, ok := s.hubs[chat.Name]; ok {
			hub = &h
			chatroom = chat
		}
	}
	s.lock.RUnlock()
	if hub == nil {
		return nil, errors.New("no hubs found")
	}
	return hub.FetchMessages(s.serverID, chatroom.ServerID, chatroom.ID)

	return nil, nil
}
