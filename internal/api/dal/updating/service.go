package updating

import (
	"github.com/google/uuid"
)

//Core logic goes in the service implementors
type Service interface {
	UpdateUser(id uuid.UUID, username string) (err error)

	UpdateChatroom(id int64, name string, serverID int64) (err error)

	UpdateMessage(id int64, text string) (err error)
}

//IO device(usually DB) calls
type Repository interface {
	UpdateUser(id uuid.UUID, username string) (err error)

	UpdateChatroom(id int64, name string, serverID int64) (err error)

	UpdateMessage(id int64, text string) (err error)
}

type service struct {
	r Repository
}

func NewService(r Repository) Service {
	return service{r}
}

func (s service) UpdateUser(id uuid.UUID, username string) error {
	return s.r.UpdateUser(id, username)
}

func (s service) UpdateChatroom(id int64, name string, serverID int64) error {
	return s.r.UpdateChatroom(id, name, serverID)
}

func (s service) UpdateMessage(id int64, text string) error {
	return s.r.UpdateMessage(id, text)
}
