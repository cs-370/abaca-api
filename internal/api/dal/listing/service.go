package listing

import (
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/syncing"
	"time"

	"github.com/google/uuid"
)

//Logic Interface
type User struct {
	ID       uuid.UUID `db:"id"`
	Username string    `db:"username"`
}

type Chatroom struct {
	ChatID   int64     `db:"id"`
	ChatName string    `db:"name"`
	ServerID uuid.UUID `db:"server_id"`
}

type Message struct {
	MessageID int64     `db:"id"`
	Text      string    `db:"msg"`
	Username  string    `db:"username"`
	SenderID  uuid.UUID `db:"sender_id"`
	ChatID    int64     `db:"chat_id"`
	SendDate  time.Time `db:"send_date"`
}

//Core logic goes in the service implementors
type Service interface {
	HubLister
	GetAllUsers() (users []User, err error)

	GetUserByID(id uuid.UUID) (username string, err error)

	GetServerID() (uuid.UUID, error)
	GetUserIDForSession(sessionID string) (userID string, err error)

	GetChatroomByID(id int64) (chatroom Chatroom, err error)

	GetMessageByID(id int64) (message Message, err error)

	GetMessagesForChatroom(chatID int64) (messages []Message, err error)

	GetAllChatroomsForUser(userID uuid.UUID) (chatrooms []Chatroom, err error)

	GetAllUSersForChatroom(chatID int64) (users []User, err error)
}

//IO device(usually DB) calls
type Repository interface {
	GetAllUsers() (users []User, err error)

	HubRepo
	GetServerID() (uuid.UUID, error)
	GetUserByID(id uuid.UUID) (username string, err error)

	GetUserIDForSession(sessionID string) (userID string, err error)

	GetChatroomByID(id int64) (chatroom Chatroom, err error)

	GetMessageByID(id int64) (message Message, err error)

	GetMessagesForChatroom(chatID int64) (messages []Message, err error)

	GetAllChatroomsForUser(userID uuid.UUID) (chatrooms []Chatroom, err error)

	GetAllUSersForChatroom(chatID int64) (users []User, err error)

	CheckForeignChatroom(chatID int64) (checkForeign bool, err error)
}

type service struct {
	r    Repository
	sync syncing.Service
}

func NewService(r Repository, sync syncing.Service) Service {
	return service{r, sync}
}

func (s service) GetAllUsers() (users []User, err error) {
	return s.r.GetAllUsers()
}

func (s service) GetUserByID(id uuid.UUID) (username string, err error) {
	return s.r.GetUserByID(id)
}

func (s service) GetServerID() (uuid.UUID, error) {
	return s.r.GetServerID()
}

func (s service) GetUserIDForSession(sessionID string) (userID string, err error) {
	return s.r.GetUserIDForSession(sessionID)
}

func (s service) GetChatroomByID(id int64) (chatroom Chatroom, err error) {
	return s.r.GetChatroomByID(id)
}

func (s service) GetMessageByID(id int64) (message Message, err error) {
	return s.r.GetMessageByID(id)
}

func (s service) GetMessagesForChatroom(id int64) (messages []Message, err error) {
	isForeign, err := s.r.CheckForeignChatroom(id)
	if err != nil {
		return nil, nil
	}
	//Access via hub
	if isForeign {
		msgs, err := s.sync.GetForeignMessages(id)
		if err != nil {
			return nil, err
		}
		var messages []Message
		for _, msg := range msgs {
			messages = append(messages, Message{
				MessageID: msg.MessageID,
				Text:      msg.Text,
				Username:  msg.Username,
				SenderID:  msg.SenderID,
				ChatID:    msg.ChatID,
				SendDate:  msg.SendDate,
			})
		}
		return messages, nil
		//Normal access
	} else {
		return s.r.GetMessagesForChatroom(id)
	}
}

func (s service) GetAllChatroomsForUser(userID uuid.UUID) (messages []Chatroom, err error) {
	return s.r.GetAllChatroomsForUser(userID)
}

func (s service) GetAllUSersForChatroom(chatID int64) (users []User, err error) {
	return s.r.GetAllUSersForChatroom(chatID)
}
