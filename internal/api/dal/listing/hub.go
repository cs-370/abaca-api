package listing

import (
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/storage/hubConnection"
)

//Listing Functionality for Hub interaction

type HubLister interface {
	GetHubs() ([]hubConnection.Hub, error)
}
type HubRepo interface {
	GetHubs() ([]hubConnection.Hub, error)
}

func (s service) GetHubs() ([]hubConnection.Hub, error) {
	return s.r.GetHubs()
}
