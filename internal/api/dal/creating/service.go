//Service that creates entities
package creating

import (
	"github.com/google/uuid"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/syncing"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/security"
)

//Core logic goes in the service implementors
type Service interface {
	CreateUser(username string, password string) (id uuid.UUID, err error)

	CreateChatRoom(name string, serverID int64) (id int64, err error)

	CreateMessage(senderID uuid.UUID, text string, chatRoomID int64) (id int64, err error)

	AddMemberToChatroom(chatID int64, userID uuid.UUID) (err error)
}

//IO device(usually DB) calls
type Repository interface {
	CreateUser(id uuid.UUID, username string, hash []byte, salt []byte) error

	CreateChatRoom(name string, serverID int64) (id int64, err error)

	CreateMessage(senderID uuid.UUID, text string, chatRoomID int64) (int64, error)

	AddMemberToChatroom(chatID int64, userID uuid.UUID) (err error)

	//Although this is mainly a listing function, creating needs it also
	CheckForeignChatroom(chatID int64) (checkForeign bool, err error)
}

type service struct {
	r    Repository
	sync syncing.Service
}

func NewService(r Repository, sync syncing.Service) Service {
	return service{
		r:    r,
		sync: sync,
	}
}

func (s service) CreateUser(username string, password string) (id uuid.UUID, err error) {
	id, err = uuid.NewRandom()
	if err != nil {
		return uuid.Nil, nil
	}

	salt, err := security.GenerateSalt()
	if err != nil {
		return uuid.Nil, nil
	}

	hash, err := security.PasswordHash(username, password, salt)
	if err != nil {
		return uuid.Nil, nil
	}

	err = s.r.CreateUser(id, username, hash, salt)
	return id, err
}

func (s service) CreateChatRoom(name string, serverID int64) (id int64, err error) {
	return s.r.CreateChatRoom(name, serverID)

}

func (s service) CreateMessage(senderID uuid.UUID, text string, chatRoomID int64) (id int64, err error) {
	isForeign, err := s.r.CheckForeignChatroom(chatRoomID)
	if err != nil {
		return 0, err
	}

	if isForeign {
		return s.sync.SendMsgToForeignChat(senderID, text, chatRoomID)

	}
	return s.r.CreateMessage(senderID, text, chatRoomID)
}

func (s service) AddMemberToChatroom(chatID int64, userID uuid.UUID) (err error) {
	return s.r.AddMemberToChatroom(chatID, userID)
}
