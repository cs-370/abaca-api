package linking

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type MockRepository struct {
	//Pretend to be a repository
	Repository
	// Repo login return
	SessionTokenRet []byte
	SessionErrRet   error
	//Salt returns
	SaltRet    []byte
	SaltErrRet error
}

func (mock MockRepository) Login(username string, passwordHash []byte) (sessionToken []byte, err error) {
	return mock.SessionTokenRet, mock.SessionErrRet
}
func (mock MockRepository) GetSaltForUser(username string) ([]byte, error) {
	return mock.SaltRet, mock.SaltErrRet
}

func TestLogin(t *testing.T) {

	cases := []struct {
		Name string
		//Inputs
		username     string
		password     string
		originServer string
		//Middleware
		repo Repository
		//Expected Outputs
		ExpectedSessionToken []byte
		ExpectedErr          error
	}{
		{
			Name:                 "HappyPath",
			username:             "u",
			password:             "p",
			repo:                 MockRepository{},
			originServer:         "",
			ExpectedSessionToken: nil,
			ExpectedErr:          nil,
		},
	}

	for _, test := range cases {
		t.Run(test.Name, func(t *testing.T) {
			s := service{r: test.repo}
			sessionID, err := s.Login(test.username, test.password, test.originServer)
			assert.Equal(t, test.ExpectedSessionToken, sessionID)
			assert.Equal(t, test.ExpectedErr, err)
		})

	}

}
