//Package for securing and connecting parts of the chat to other parts
package linking

import (
	"crypto/rsa"
	"errors"
	"github.com/google/uuid"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/listing"

	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/syncing"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/security"
)

//Core logic layer interface
type Service interface {
	//Log into this API chat service
	Login(username string, password string, originServer string) (sessionToken []byte, err error)

	//Sync a hub to this API
	BindToHub(address string, hubName string) (hubID int64, err error)

	//Link another API to this one.
	RegisterAPI(hubName string, serverName uuid.UUID) (err error)

	//Share a chatroom with another server
	ShareChatroom(hubName string, remoteServer uuid.UUID, chatroomID int64) error
}

//Storage interface, such as db.
type Repository interface {
	//Match the user and password for the given server
	Login(username string, passwordHash []byte) (sessionToken []byte, err error)

	//Get the salt for a given user.
	GetSaltForUser(username string) ([]byte, error)

	//Store a Hub for this server.
	RegisterHub(address string, name string) (hubID int64, err error)
	//Store a secondary API server
	RegisterAPI(hubName string, apiID uuid.UUID, sessionKey []byte, key *rsa.PublicKey) error
}

//Implementation of the Service Layer
type service struct {
	r      Repository
	sync   syncing.Service
	lister listing.Service
}

func NewService(r Repository, sync syncing.Service, lister listing.Service) Service {
	return service{
		r:      r,
		sync:   sync,
		lister: lister,
	}
}

//Log into this API chat service
func (s service) Login(username string, password string, originServer string) (sessionToken []byte, err error) {

	if originServer != "" {
		return nil, errors.New("foreign login not implemented")
	}

	var salt []byte
	salt, err = s.r.GetSaltForUser(username)
	if err != nil {
		return nil, err
	}
	var hash []byte
	hash, err = security.PasswordHash(username, password, salt)
	if err != nil {
		return nil, err
	}
	return s.r.Login(username, hash)

}

//Sync a hub to this API
func (s service) BindToHub(address string, hubName string) (hubID int64, err error) {
	err = s.sync.RegisterNewHub(hubName, address)
	if err != nil {
		return 0, err
	}
	return s.r.RegisterHub(address, hubName)

}

//Link another API to this one.
func (s service) RegisterAPI(hubName string, serverID uuid.UUID) error {
	key, pubkey, err := s.sync.LinkAPI(hubName, serverID)
	if err != nil {
		return err
	}
	return s.r.RegisterAPI(hubName, serverID, key, pubkey)
}

//Share a chatroom with another server
func (s service) ShareChatroom(hubName string, remoteServer uuid.UUID, chatroomID int64) error {
	//Verify chatroom exists
	_, err := s.lister.GetChatroomByID(chatroomID)
	if err != nil {
		return err
	}
	return s.sync.ShareChatroom(hubName, remoteServer, chatroomID)
}
