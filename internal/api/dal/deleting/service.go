package deleting

import (
	"github.com/google/uuid"
)

//Core logic goes in the service implementors
type Service interface {
	DeleteUser(id uuid.UUID) (err error)

	DeleteChatroom(id int64) (err error)

	DeleteMessage(id int64) (err error)
}

//IO device(usually DB) calls
type Repository interface {
	DeleteUser(id uuid.UUID) (err error)

	DeleteChatroom(id int64) (err error)

	DeleteMessage(id int64) (err error)
}

type service struct {
	r Repository
}

func NewService(r Repository) Service {
	return service{r}
}

func (s service) DeleteUser(id uuid.UUID) (err error) {
	return s.r.DeleteUser(id)
}

func (s service) DeleteChatroom(id int64) (err error) {
	return s.r.DeleteChatroom(id)
}

func (s service) DeleteMessage(id int64) (err error) {
	return s.r.DeleteMessage(id)
}
