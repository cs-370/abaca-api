package postgres

import (
	"database/sql"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/storage/hubConnection"

	"github.com/google/uuid"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/listing"
)

func (conn Connection) GetAllUsers() (users []listing.User, err error) {
	meta := []listing.User{}
	err = conn.db.Select(&meta, `SELECT id, username FROM abaca.users`)
	return meta, err
}

func (conn Connection) GetServerID() (uuid.UUID, error) {
	var buffer string
	err := conn.db.Get(&buffer, "SELECT id FROM abaca.servers WHERE name = 'self'")
	if err != nil {
		return uuid.Nil, err
	}
	return uuid.Parse(buffer)
}

func (conn Connection) GetUserIDForSession(sessionID string) (userID string, err error) {
	err = conn.db.Get(&userID, "SELECT user_id FROM abaca.sessions WHERE id = $1 and expiry > current_timestamp limit 1", sessionID)
	if err != nil {
		return "", err
	}
	return
}

func (conn Connection) GetUserByID(id uuid.UUID) (username string, error error) {
	var user string
	err := conn.db.Get(&user, `SELECT username FROM abaca.users WHERE id = $1`, id.String())
	return user, err
}

func (conn Connection) GetChatroomByID(id int64) (chatroom listing.Chatroom, err error) {
	meta := listing.Chatroom{}
	err = conn.db.Get(&meta, `SELECT id, name, server_id FROM abaca.chatrooms WHERE id = $1`, id)
	return meta, err
}

func (conn Connection) GetMessageByID(id int64) (message listing.Message, err error) {
	meta := listing.Message{}
	err = conn.db.Get(&meta, `SELECT id, msg, sender_id, chat_id, send_date FROM abaca.messages WHERE id = $1`, id)
	return meta, err
}

func (conn Connection) GetMessagesForChatroom(chatID int64) (messages []listing.Message, err error) {
	meta := []listing.Message{}
	err = conn.db.Select(&meta, `SELECT abaca.messages.id, msg, username, sender_id, chat_id, send_date
								 FROM abaca.chatrooms 
								 LEFT JOIN abaca.messages ON abaca.chatrooms.id = abaca.messages.chat_id
								 LEFT JOIN abaca.users ON abaca.messages.sender_id = abaca.users.id
								 WHERE abaca.chatrooms.id = $1
								 ORDER BY abaca.messages.send_date ASC`, chatID)
	return meta, err
}

// Hub Logic

func (conn Connection) GetHubs() (hubs []hubConnection.Hub, err error) {
	err = conn.db.Select(&hubs, "SELECT * FROM abaca.hubs")
	return hubs, err
}
func (conn Connection) GetAllChatroomsForUser(userID uuid.UUID) (chatrooms []listing.Chatroom, err error) {
	meta := []listing.Chatroom{}
	err = conn.db.Select(&meta, `SELECT id, name, server_id
								 FROM abaca.chat_members
								 LEFT JOIN abaca.chatrooms ON abaca.chat_members.chat_id = abaca.chatrooms.id
								 WHERE abaca.chat_members.user_id = $1
								 ORDER BY abaca.chatrooms.name`, userID)
	return meta, err
}

func (conn Connection) GetAllUSersForChatroom(chatID int64) (users []listing.User, err error) {
	meta := []listing.User{}
	err = conn.db.Select(&meta, `SELECT id, username
								 FROM abaca.chat_members
								 LEFT JOIN abaca.users ON abaca.chat_members.user_id = abaca.users.id
								 WHERE abaca.chat_members.chat_id = $1
								 ORDER BY abaca.users.username`, chatID)
	return meta, err
}

func (conn Connection) CheckForeignChatroom(chatID int64) (checkForeign bool, err error) {
	var foreignID sql.NullInt64

	err = conn.db.Get(&foreignID, `SELECT foreign_id FROM abaca.chatrooms WHERE abaca.chatrooms.id = $1`, chatID)
	if err != nil {
		return false, err
	}

	if foreignID.Valid {
		return true, nil
	} else {
		return false, nil
	}
}
