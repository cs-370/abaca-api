package postgres

import "github.com/google/uuid"

func (conn Connection) UpdateUser(id uuid.UUID, username string) (err error) {
	_, err = conn.db.Exec(`UPDATE abaca.users
						   SET username = $2, last_refresh = NOW()
						   WHERE id = $1`, id, username)

	return err
}

func (conn Connection) UpdateChatroom(id int64, name string, serverID int64) (err error) {
	_, err = conn.db.Exec(`UPDATE abaca.chatrooms
						   SET name = $2, server_id = $3
						   WHERE id = $1`, id, name, serverID)
	return err
}

func (conn Connection) UpdateMessage(id int64, text string) (err error) {
	_, err = conn.db.Exec(`UPDATE abaca.messages
						   SET text = $2, send_date = NOW()
						   WHERE id = $1`, id, text)
	return err
}
