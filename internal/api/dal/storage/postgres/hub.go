package postgres

import (
	"github.com/google/uuid"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/storage/hubConnection"
)

//Postgres file for functionality specific to the hub

//Find a hub for a given server
func (conn Connection) GetHubsForServer(serverID uuid.UUID) (hubs []hubConnection.Hub, err error) {
	err = conn.db.Select(&hubs, `SELECT id, name, address, hub_id, server_id
			from abaca.hubs LEFT JOIN abaca.hub_servers on hubs.id = hub_servers.hub_id
			where server_id = $1`, serverID)
	return hubs, err
}

//Encryption/Decryption key for a server
func (conn Connection) GetServerSecret(serverID string) (secret []byte, err error) {
	err = conn.db.Get(&secret, `SELECT secret from abaca.servers where id = $1 limit 1`, serverID)
	return
}

//Insert a chatroom being shared elsewhere
func (conn Connection) AddForeignChatroom(serverID uuid.UUID, foreignID int64) error {
	_, err := conn.db.Exec(`INSERT INTO abaca.chatrooms (name, server_id,foreign_id)
							 VALUES ($1, $2,$3)`, serverID.String(), serverID, foreignID)
	return err
}

//Send a message to a chatroom not on this server
func (conn Connection) SendForeignMessage(senderID uuid.UUID, senderName string, serverID uuid.UUID, text string, chatRoomID int64) (messageID int64, err error) {
	var id int64
	_, err = conn.db.Exec(`INSERT INTO abaca.users (id, username, server_id, last_refresh)
		VALUES ($1,$2,$3,current_timestamp) on conflict (id) DO UPDATE set last_refresh = current_timestamp
	`, senderID.String(), senderName, serverID.String())
	if err != nil {
		return 0, err
	}

	err = conn.db.QueryRow(`INSERT INTO abaca.messages (msg, sender_id, chat_id, send_date)
							 VALUES ($1, $2, $3, NOW())
							 RETURNING id`, text, senderID, chatRoomID).Scan(&id)
	return id, err
}

//Derives ForeignChat Information from its local chatID
func (conn Connection) GetForeignChatRoomInfo(chatID int64) (foreignChat []hubConnection.ForeignChat, err error) {

	err = conn.db.Select(&foreignChat, `SELECT DISTINCT foreign_id,chatrooms.server_id,hubs.name from abaca.chatrooms
			LEFT JOIN abaca.hub_servers on chatrooms.server_id = hub_servers.server_id
			LEFT JOIN abaca.hubs on hub_servers.hub_id = hubs.id
    		where chatrooms.id = $1`, chatID)
	return
}

func (conn Connection) GetMessagesForForeignChatroom(id int64) ([]hubConnection.Message, error) {
	messages, err := conn.GetMessagesForChatroom(id)
	var msgs []hubConnection.Message
	if err != nil {
		return nil, err
	}
	for _, message := range messages {
		msgs = append(msgs, hubConnection.Message{
			MessageID: message.MessageID,
			Text:      message.Text,
			Username:  message.Username,
			SenderID:  message.SenderID,
			ChatID:    message.ChatID,
			SendDate:  message.SendDate,
		})
	}
	return msgs, err
}
