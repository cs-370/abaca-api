package postgres

import "github.com/google/uuid"

func (conn Connection) CreateUser(id uuid.UUID, username string, hash []byte, salt []byte) (err error) {
	_, err = conn.db.Exec(`INSERT INTO abaca.users (id, username, password_hash, salt) 
						    VALUES ($1, $2, $3, $4)`, id, username, hash, salt)
	return err
}

func (conn Connection) CreateChatRoom(name string, serverID int64) (chatID int64, err error) {
	var id int64
	err = conn.db.QueryRow(`INSERT INTO abaca.chatrooms (name, server_id)
							 VALUES ($1, $2)
							 RETURNING id`, name, serverID).Scan(&id)
	return id, err
}

func (conn Connection) CreateMessage(senderID uuid.UUID, text string, chatRoomID int64) (messageID int64, err error) {
	var id int64
	err = conn.db.QueryRow(`INSERT INTO abaca.messages (msg, sender_id, chat_id, send_date)
							 VALUES ($1, $2, $3, NOW())
							 RETURNING id`, text, senderID, chatRoomID).Scan(&id)
	return id, err
}

func (conn Connection) AddMemberToChatroom(chatID int64, userID uuid.UUID) (err error) {
	_, err = conn.db.Exec(`INSERT INTO abaca.chat_members (chat_id, user_id)
						   VALUES ($1, $2)`, chatID, userID)
	return err
}
