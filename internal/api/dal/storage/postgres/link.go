package postgres

import (
	"bytes"
	"crypto/rsa"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/google/uuid"
)

//Match the user and password for the given server
func (conn Connection) Login(username string, passwordHash []byte) (sessionToken []byte, err error) {
	type auth struct {
		UserID   []byte `db:"id"`
		Password []byte `db:"password_hash"`
	}
	fmt.Println(hex.EncodeToString(passwordHash))
	meta := []auth{}
	err = conn.db.Select(&meta, "SELECT id,password_hash from abaca.users where username = $1", username)
	if err != nil {
		return nil, err
	}
	for _, user := range meta {
		if bytes.Equal(user.Password, passwordHash) {
			id, err := conn.generateSession(user.UserID)
			return []byte(id.String()), err
		}
	}
	return nil, errors.New("invalid password")
}

func (conn Connection) generateSession(userID []byte) (sessionToken uuid.UUID, err error) {

	sessionToken, err = uuid.NewRandom()
	if err != nil {
		return
	}
	query := "INSERT INTO abaca.sessions (id,user_id,expiry,created_at) VALUES "
	query += " ($1,$2,((now()+ INTERVAL '1' HOUR) at time zone 'utc'),(now() at time zone 'utc'))"
	_, err = conn.db.Exec(query, sessionToken.String(), string(userID))
	return

}

func (conn Connection) GetSaltForUser(username string) ([]byte, error) {
	saltTable := [][]byte{}
	err := conn.db.Select(&saltTable, "SELECT salt from abaca.users where username = $1", username)
	if len(saltTable) > 0 {
		return saltTable[0], err
	}
	return nil, err
}

//Store a Hub for this server.
func (conn Connection) RegisterHub(address string, name string) (hubID int64, err error) {
	err = conn.db.Get(&hubID, `INSERT INTO abaca.hubs (id,name,address) VALUES ((SELECT coalesce(max(id),0)+1 from abaca.hubs),$1,$2) RETURNING id`, name, address)
	return
}

//Store a secondary API server
func (conn Connection) RegisterAPI(hubName string, apiID uuid.UUID, sessionKey []byte, key *rsa.PublicKey) error {

	raw, err := json.Marshal(key)
	if err != nil {
		return err
	}
	_, err = conn.db.Exec(
		`INSERT INTO abaca.servers (id,name, public_key, secret) 
				VALUES ($1,'',$2,$3) ON CONFLICT (id)
				DO UPDATE SET public_key = $2, secret = $3`,
		apiID.String(), raw, sessionKey)

	if err != nil {
		return err
	}
	_, err = conn.db.Exec(`INSERT INTO abaca.hub_servers (hub_id,server_id) ((SELECT id as hub_id,$2 as server_id from abaca.hubs where name = $1))`, hubName, apiID)
	return err
}
