DROP SCHEMA if exists abaca CASCADE;
CREATE SCHEMA abaca;

CREATE TABLE abaca.servers(
    id uuid primary key,
    name text not null,
    public_key bytea,
    secret bytea
);

CREATE TABLE abaca.hubs(
    id serial primary key,
    name text,
    address text not null
);

-- Links Hubs to servers, allowing multiple hubs to provide access to a server
CREATE TABLE abaca.hub_servers(
    hub_id int,
    server_id uuid
);

CREATE TABLE abaca.users(
    --Unique id for the user
    id uuid primary key,
    --Name for the user
    username text not null,
     -- Hash of the password. Salted differently between servers.
    password_hash bytea,
    -- Salt for the password
    salt bytea,
    --Server the user belongs to.
    server_id uuid,
    --Date a foreign user was last updated, null for local users
    last_refresh timestamp,

    CONSTRAINT fk_server FOREIGN KEY(server_id) REFERENCES abaca.servers(id)
);

CREATE TABLE abaca.sessions(
    id uuid primary key,
    user_id uuid,
    expiry timestamp not null,
    created_at timestamp not null,
    notes text
);


CREATE TABLE abaca.chatrooms(
    id serial primary key,
    name text,
    server_id uuid,
    --ID for this server remotely
    foreign_id int,
    CONSTRAINT fk_server FOREIGN KEY (server_id) REFERENCES abaca.servers(id)
);

CREATE TABLE abaca.chat_members(
    chat_id int,
    user_id uuid,
   CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES abaca.users(id),
   CONSTRAINT fk_chat FOREIGN KEY(chat_id) REFERENCES abaca.chatrooms(id)
);

CREATE TABLE abaca.messages(
    id serial primary key,
    msg text,
    sender_id uuid,
    chat_id int,
    send_date timestamp not null  default current_timestamp,
    CONSTRAINT fk_sender FOREIGN KEY(sender_id) REFERENCES abaca.users(id)
);