do $$
declare
    serverID uuid;
    adminUserID uuid;
begin
    serverID = gen_random_uuid();
    adminUserID = gen_random_uuid();

-- Specify this server in the server list
INSERT INTO abaca.servers (id,name) VALUES
    (serverID,'self');


INSERT INTO abaca.users(id,username,password_hash,salt,server_id,last_refresh) VALUES
(
    adminUserID,'admin',
    --hex encoding of password admin
    decode('966b819c95056fbe6783a549cb600df5bb22f6a1fb60c57f0c19e925944098b28adb7eb1c7ea4551ecabe766b84e98bdb20bd54732c8a4b62ba456eacb4b012b','hex'),
    --hex encoding of salt used for password
    decode('00000000000000000000000000000000','hex'),
    serverID,
    null
);

INSERT INTO abaca.chatrooms(id, name, server_id) VALUES (
    1,
    'Test', 
    serverID
);

INSERT INTO abaca.chatrooms(id, name, server_id) VALUES (
    2,
    'Test 2',
    serverID
);

INSERT INTO abaca.chat_members(chat_id, user_id) VALUES (
    1,
    adminUserID
);

end $$