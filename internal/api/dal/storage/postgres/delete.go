package postgres

import "github.com/google/uuid"

func (conn Connection) DeleteUser(id uuid.UUID) error {
	_, err := conn.db.Exec(`DELETE FROM abaca.users WHERE id = $1`, id)
	return err
}

func (conn Connection) DeleteChatroom(id int64) error {
	_, err := conn.db.Exec(`DELETE FROM abaca.chatrooms WHERE id = $1`, id)
	return err
}

func (conn Connection) DeleteMessage(id int64) error {
	_, err := conn.db.Exec(`DELETE FROM abaca.messages WHERE id = $1`, id)
	return err
}
