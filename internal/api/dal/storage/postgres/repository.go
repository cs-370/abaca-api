package postgres

import (
	"github.com/jmoiron/sqlx"

	//Implicit postgres driver import
	_ "github.com/lib/pq"
)

type Connection struct {
	db *sqlx.DB
}

func NewConnection(url string) (Connection, error) {
	conn, err := sqlx.Open("postgres", url)
	if err != nil {
		return Connection{db: conn}, err
	}
	x := []int{}
	err = conn.Select(&x, "Select 1 as x")
	return Connection{db: conn}, err
}

//Only put interface stubs below, separate implementation into their own files
