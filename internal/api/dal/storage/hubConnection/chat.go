package hubConnection

import (
	"github.com/google/uuid"
	"time"
)

//Wrapper for information about a foreign chatroom
type ForeignChat struct {
	ID       int64     `db:"foreign_id"`
	ServerID uuid.UUID `db:"server_id"`
	Name     string    `db:"name"`
}

type Message struct {
	MessageID int64     `db:"id"`
	Text      string    `db:"msg"`
	Username  string    `db:"username"`
	SenderID  uuid.UUID `db:"sender_id"`
	ChatID    int64     `db:"chat_id"`
	SendDate  time.Time `db:"send_date"`
}
