// Hub package for clients, connects to the hub to get information
package hubConnection

import (
	"bufio"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"time"

	"github.com/google/uuid"
	"gitlab.com/cs-370/abaca-api.git/internal/hub"
)

//Database functions the hub needs access to
type Repository interface {
	RegisterAPI(hubName string, apiID uuid.UUID, sessionKey []byte, key *rsa.PublicKey) error
	GetServerSecret(serverID string) (secret []byte, err error)
	AddForeignChatroom(serverID uuid.UUID, foreignID int64) error
	GetMessagesForForeignChatroom(chatID int64) (messages []Message, err error)

	SendForeignMessage(senderID uuid.UUID, senderName string, serverID uuid.UUID, text string, chatRoomID int64) (messageID int64, err error)
	GetHubsForServer(serverID uuid.UUID) (hubs []Hub, err error)

	//Required for sending messages
	GetForeignChatRoomInfo(chatID int64) ([]ForeignChat, error)
	GetUserByID(id uuid.UUID) (username string, error error)
}

//Hub object for the db
type Hub struct {
	ID      int64  `db:"id"`
	Name    string `db:"name"`
	Address string `db:"address"`
}

//TCP Connection
type HubConnection struct {
	name       string
	address    string
	conn       net.Conn
	key        *rsa.PrivateKey
	db         Repository
	lastUpdate time.Time
}

func NewHubConnection(name string, addr string, key *rsa.PrivateKey, db Repository) HubConnection {
	return HubConnection{
		name:       name,
		address:    addr,
		conn:       nil,
		key:        key,
		db:         db,
		lastUpdate: time.Time{},
	}
}

//Registers the API server with a hub.
func (hubConn *HubConnection) Register(id uuid.UUID, key *rsa.PublicKey) error {
	//Connect to the hub
	conn, err := net.Dial("tcp", hubConn.address)

	tcpConn, ok := (conn).(*net.TCPConn)
	if ok {
		_ = tcpConn.SetKeepAlive(true)
		_ = tcpConn.SetKeepAlivePeriod(time.Second)
	}

	if err != nil {
		return err
	}
	//Introduce this API to the hub
	msg := []byte{
		byte(hub.Hello),
		byte(hub.DefaultMinor),
	}
	rawID, _ := id.MarshalText()
	msg = append(msg, rawID...)
	// 36 + 2 header bytes
	if len(msg) != 38 {
		return errors.New("Invalid server id")
	}
	//Server Public key
	rawKey := x509.MarshalPKCS1PublicKey(key)
	msg = append(msg, rawKey...)
	//Footer
	msg = append(msg, 0xAB, 0xAC)

	_, err = conn.Write(msg)
	if err != nil {
		return err
	}

	msg = make([]byte, 64)
	_, err = conn.Read(msg)
	if string(msg) == id.String() {
		return errors.New(fmt.Sprint("handshake failed with address ", hubConn.address))
	}
	log.Print("Successful Handshake with ", hubConn.address)
	hubConn.lastUpdate = time.Now()
	hubConn.conn = conn
	return err

}

func (hubConn *HubConnection) FindServers() ([]string, error) {
	conn, err := net.Dial("tcp", hubConn.address)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	msg := []byte{byte(hub.Query), byte(hub.QueryServers), 0xAB, 0xAC}

	_, err = conn.Write(msg)
	if err != nil {
		return nil, err
	}
	length := make([]byte, 1)
	_, err = conn.Read(length)
	if err != nil {
		return nil, err
	}
	response := make([]byte, 37*length[0])
	_, err = conn.Read(response)
	if err != nil {
		return nil, err
	}
	var uuids []string
	for c := 0; c < len(response)-36; c += 37 {
		id := string(response[c : c+36])
		_, err = uuid.Parse(id)
		if err != nil {
			return uuids, err
		}
		uuids = append(uuids, id)
	}

	return uuids, err
}

//Get the public key of a hub
func (hubConn *HubConnection) GetPublicKey(serverID []byte) (*rsa.PublicKey, error) {
	conn, err := net.Dial("tcp", hubConn.address)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	msg := append([]byte{byte(hub.Query), byte(hub.QueryServerInfo)}, serverID...)
	msg = append(msg, 0xAB, 0xAC)
	_, err = conn.Write(msg)
	if err != nil {
		return nil, err
	}
	data, err := bufio.NewReader(conn).ReadSlice('\n')
	if err != nil {
		return nil, err
	}
	var key rsa.PublicKey
	err = json.Unmarshal(data, &key)
	return &key, err
}

//Checks for messages from foreign servers.
//On exits, reattempts to connect are made
func (hubConn *HubConnection) sync() {
	var message []byte
	for {
		//Max frame size
		buffer := make([]byte, 1500)
		fmt.Println("Waiting for information from the hub")
		_ = hubConn.conn.SetReadDeadline(time.Now().Add(time.Minute))
		count, err := hubConn.conn.Read(buffer)
		_ = hubConn.conn.SetReadDeadline(time.Time{})
		if err != nil {

			if err == io.EOF {
				continue
			} else {
				fmt.Println(err)
			}
			return
		}
		fmt.Println("Read ", count, " bytes from ", hubConn.conn.RemoteAddr())
		//Collect the message
		message = append(message, buffer...)
		//Try to parse the message
		switch resp := hubConn.parseMessage(message).(type) {
		//Reply was calculated
		case []byte:
			_, err := hubConn.conn.Write(resp)
			if err != nil {
				fmt.Println(err)
				message = nil
				return
			}
			message = nil
		//Keep reading data on true
		case bool:
			if resp == false {
				message = nil
			}
		default:
			message = nil

		}

	}
}

//Check for messages from foreign servers
//Assumes registration has occurred.
func (hubConn *HubConnection) Sync(id uuid.UUID, pubkey *rsa.PublicKey) {
	for idx := 10; idx > 0; idx-- {
		if hubConn.conn != nil {
			hubConn.sync()
		}
		err := hubConn.Register(id, pubkey)
		if err != nil {
			hubConn.conn = nil
		} else {
			idx = 10
		}

	}
}
