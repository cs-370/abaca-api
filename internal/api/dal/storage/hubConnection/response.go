package hubConnection

import (
	"encoding/binary"
	"encoding/json"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/cs-370/abaca-api.git/internal/hub"
	"gitlab.com/cs-370/abaca-api.git/internal/hub/message"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/security"
)

//Parses a message from another server
func (hubConn *HubConnection) parseMessage(msg []byte) interface{} {
	if len(msg) < 4 {
		return true
	}
	if hub.RequestMajorByte(msg[0]) != hub.Forward {
		return []byte{}
	}
	switch hub.RequestMinorByte(msg[1]) {
	case hub.KeyExchange:
		return hubConn.parseKeyExchange(msg)
	case hub.ShareChatroom:
		return hubConn.parseShareChatroom(msg)

	case hub.SendChatMsg:
		return hubConn.parseSendMsg(msg)
	case hub.ListMessagesRequest:
		return hubConn.parseListMessage(msg)
	}

	return []byte{}
}

//Key Exchange Message Format:
// 2 byte header
// 36 byte sender ID
// 36 byte receiver ID
// 8 byte content length big endian uint64
// X bytes content data that is the secret
// 2 byte footer
func (hubConn *HubConnection) parseKeyExchange(msg []byte) interface{} {
	if len(msg) < 48 {
		return true
	}
	//Read the key exchange request
	senderID, err := uuid.Parse(string(msg[2:38]))
	if err != nil {
		return []byte{}
	}
	contentLength := binary.BigEndian.Uint64(msg[74:82])

	rawBody := msg[82 : 82+contentLength]
	body := message.KeyExchangeBody{}
	json.Unmarshal(rawBody, &body)

	plaintext, err := security.DecryptWithPrivateKey(hubConn.key, body.CipherText)
	if err != nil {
		return []byte{}
	}
	encryptedSecret, err := security.EncryptWithPublicKey(body.Key, plaintext)
	if err != nil {
		return []byte{}
	}

	err = hubConn.db.RegisterAPI(hubConn.name, senderID, plaintext, body.Key)
	if err != nil {
		return []byte{}
	}

	header := msg[0:2]
	header[1] = byte(hub.ForwardReply)
	//New content length
	contentLengthHeader := make([]byte, 8)
	binary.BigEndian.PutUint64(contentLengthHeader, uint64(len(encryptedSecret)))
	//Swap the sender and the recipient
	msg = append(msg[38:74], msg[2:38]...)
	//Prepend the header
	msg = append(header, msg...)
	//Append the content length and secret
	msg = append(msg, contentLengthHeader...)
	msg = append(msg, encryptedSecret...)
	//Add footer
	msg = append(msg, 0xAB, 0xAC)

	return msg
}

//Share Chatroom Message Format
// 2 byte header
// 36 byte sender ID
// 36 byte receiver ID
// 8 byte content length big endian uint64
// X bytes content data that is the encrypted chat ID
// 2 byte footer
func (hubConn *HubConnection) parseShareChatroom(msg []byte) interface{} {
	if len(msg) < 48 {
		return true
	}
	//Read the key exchange request
	senderID, err := uuid.Parse(string(msg[2:38]))
	if err != nil {
		return []byte{}
	}
	contentLength := binary.BigEndian.Uint64(msg[74:82])

	secret, err := hubConn.db.GetServerSecret(senderID.String())
	if err != nil {
		return []byte(fmt.Sprint(err.Error()))
	}

	//Read message
	rawBody := msg[82 : 82+contentLength]
	plaintext, err := security.Decrypt(secret, rawBody)
	body := int64(0)
	err = json.Unmarshal(plaintext, &body)
	if err != nil {
		return []byte(fmt.Sprint(err.Error()))
	}

	//Update db
	err = hubConn.db.AddForeignChatroom(senderID, body)
	if err != nil {
		return err
	}
	//Response
	header := msg[0:2]
	header[1] = byte(hub.ForwardReply)
	//New content length
	contentLengthHeader := make([]byte, 8)
	binary.BigEndian.PutUint64(contentLengthHeader, uint64(0))
	//Swap the sender and the recipient
	msg = append(msg[38:74], msg[2:38]...)
	//Prepend the header
	msg = append(header, msg...)
	//Append the content length and secret
	msg = append(msg, contentLengthHeader...)
	//Add footer
	msg = append(msg, 0xAB, 0xAC)

	return msg
}

//Share Chatroom Message Format
// 2 byte header
// 36 byte sender ID
// 36 byte receiver ID
// 8 byte content length big endian uint64
// X bytes content data that is the encrypted message
// 2 byte footer
func (hubConn *HubConnection) parseSendMsg(msg []byte) interface{} {
	//Read the key exchange request
	senderID, err := uuid.Parse(string(msg[2:38]))
	if err != nil {
		return []byte{}
	}
	receiverID, err := uuid.Parse(string(msg[38:74]))
	if err != nil {
		return []byte{}
	}
	contentLength := binary.BigEndian.Uint64(msg[74:82])

	secret, err := hubConn.db.GetServerSecret(senderID.String())
	if err != nil {
		return []byte(fmt.Sprint(err.Error()))
	}

	//Read message
	rawBody := msg[82 : 82+contentLength]
	plaintext, err := security.Decrypt(secret, rawBody)
	body := message.Message{}
	err = json.Unmarshal(plaintext, &body)
	if err != nil {
		return []byte(fmt.Sprint(err.Error()))
	}

	//Update db
	id, err := hubConn.db.SendForeignMessage(body.SenderID, body.Username, receiverID, body.Text, body.ChatID)
	resp, err := json.Marshal(id)
	if err != nil {
		return []byte(fmt.Sprint(err.Error()))
	}

	raw, err := security.Encrypt(secret, resp)

	if err != nil {
		return err
	}
	//Response
	header := msg[0:2]
	header[1] = byte(hub.ForwardReply)
	//New content length
	contentLengthHeader := make([]byte, 8)
	binary.BigEndian.PutUint64(contentLengthHeader, uint64(len(raw)))
	//Swap the sender and the recipient
	msg = append(msg[38:74], msg[2:38]...)
	//Prepend the header
	msg = append(header, msg...)
	//Append the content length and secret
	msg = append(msg, contentLengthHeader...)
	msg = append(msg, raw...)
	//Add footer
	msg = append(msg, 0xAB, 0xAC)

	return msg
}

func (hubConn *HubConnection) parseListMessage(msg []byte) interface{} {
	if len(msg) < 48 {
		return true
	}

	sender, err := uuid.Parse(string(msg[2:38]))
	if err != nil {
		return []byte{}
	}
	receiver, err := uuid.Parse(string(msg[38:74]))
	if err != nil {
		return []byte{}
	}

	contentLength := binary.BigEndian.Uint64(msg[74:82])

	secret, err := hubConn.db.GetServerSecret(sender.String())
	if err != nil {
		return err
	}

	rawBody := msg[82 : 82+contentLength]
	plaintext, err := security.Decrypt(secret, rawBody)
	if err != nil {
		return err
	}

	body := int64(0)
	json.Unmarshal(plaintext, &body)

	messages, err := hubConn.db.GetMessagesForForeignChatroom(body)

	if err != nil {
		return err
	}

	//Header
	header := []byte{byte(hub.Forward), byte(hub.ListMessagesResponse)}
	//Body
	rawBody, err = json.Marshal(&messages)
	if err != nil {
		return nil
	}

	//Content length
	contentLength = uint64(len(rawBody))
	contentLengthByteString := make([]byte, 8)
	binary.BigEndian.PutUint64(contentLengthByteString, contentLength)
	//Assemble the pieces
	msg = append(header, []byte(receiver.String())...)
	msg = append(msg, []byte(sender.String())...)
	msg = append(msg, contentLengthByteString...)
	msg = append(msg, rawBody...)
	msg = append(msg, 0xAB, 0xAC)

	return msg
}
