package hubConnection

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"encoding/binary"
	"encoding/json"
	"errors"
	"net"
	"time"

	"github.com/google/uuid"
	"gitlab.com/cs-370/abaca-api.git/internal/hub/message"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/security"
)

//Forward Messages, which wrap the messages sent below have the following format:
//	2 byte header
//  36 byte server ID of the sender
//  36 byte server ID of the receiver
//  8 byte big endian uint64 for the size of the message
//  X bytes specified above
//	2 byte footer
// The forwarded message has the same format as above

//File for requests to other servers over a hubConnection
//Sends a key exchange to another server
func (hubConn *HubConnection) RequestKeyExchange(remoteID uuid.UUID, remotePublicKey *rsa.PublicKey, localID uuid.UUID, localKey *rsa.PrivateKey) (sessionKey []byte, err error) {
	var conn net.Conn
	conn, err = net.Dial("tcp", hubConn.address)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	//Create a secret
	sessionKey = make([]byte, 16)
	_, err = rand.Read(sessionKey)
	if err != nil {
		return nil, err
	}
	//Encrypt it with the public key
	ciphertext, err := security.EncryptWithPublicKey(remotePublicKey, sessionKey)
	if err != nil {
		return nil, err
	}
	msg, err := message.KeyExchangeRequest(
		[]byte(localID.String()), []byte(remoteID.String()),
		ciphertext, &hubConn.key.PublicKey)
	//Send MSG
	_, err = conn.Write(msg)
	if err != nil {
		return nil, err
	}
	//Read response
	buffer := make([]byte, len(msg))
	_, err = conn.Read(buffer)
	if err != nil {
		return nil, err
	}
	contentLength := buffer[74:82]
	length := binary.BigEndian.Uint64(contentLength)
	//Ensure secret was properly sent back
	cipher := buffer[82 : 82+length]
	plaintext, err := security.DecryptWithPrivateKey(localKey, cipher)
	if err != nil {
		return nil, err
	}
	if !bytes.Equal(plaintext, sessionKey) {
		return nil, errors.New("invalid secret")
	}
	//Return secret
	return
}

//Share a chatroom with another server
func (hubConn *HubConnection) ShareChatroom(localID uuid.UUID, remoteID uuid.UUID, chatID int64, sessionKey []byte) error {
	conn, err := net.Dial("tcp", hubConn.address)
	if err != nil {
		return err
	}
	defer conn.Close()
	msg, err := message.ShareChatroomRequest([]byte(localID.String()), []byte(remoteID.String()), chatID, sessionKey)
	if err != nil {
		return err
	}
	//Send share chatroom request
	_, err = conn.Write(msg)
	if err != nil {
		return err
	}
	resp := make([]byte, 100)
	_ = conn.SetReadDeadline(time.Now().Add(time.Minute))
	_, err = conn.Read(resp)
	_ = conn.SetReadDeadline(time.Time{})
	if err != nil {
		return err
	}

	return nil
}

//Send a message to a foreign chatroom
func (hubConn *HubConnection) SendMsg(localID uuid.UUID, remoteID uuid.UUID, messageData message.Message, sessionKey []byte) (int64, error) {
	conn, err := net.Dial("tcp", hubConn.address)
	if err != nil {
		return 0, err
	}
	defer conn.Close()
	msg, err := message.SendChatMessageRequest([]byte(localID.String()), []byte(remoteID.String()), messageData, sessionKey)
	if err != nil {
		return 0, err
	}
	//Send message
	_, err = conn.Write(msg)
	if err != nil {
		return 0, err
	}
	//Read reply
	reply := make([]byte, 1500)
	_ = conn.SetReadDeadline(time.Now().Add(time.Minute))
	_, err = conn.Read(reply)
	_ = conn.SetReadDeadline(time.Time{})
	if err != nil {
		return 0, err
	}

	contentLength := reply[74:82]
	length := binary.BigEndian.Uint64(contentLength)
	if uint64(len(reply)) < length {
		buffer := make([]byte, length-uint64(len(reply))+2)
		_ = conn.SetReadDeadline(time.Now().Add(time.Minute))
		_, err = conn.Read(reply)
		_ = conn.SetReadDeadline(time.Time{})
		if err != nil {
			return 0, err
		}
		reply = append(reply, buffer...)
	}

	body := reply[82 : length+82]
	plaintext, err := security.Decrypt(sessionKey, body)
	if err != nil {
		return 0, err
	}
	var id int64
	err = json.Unmarshal(plaintext, &id)
	return id, err
}

func (hubConn *HubConnection) FetchMessages(localID uuid.UUID, remoteID uuid.UUID, chatID int64) ([]Message, error) {

	secret, err := hubConn.db.GetServerSecret(remoteID.String())
	if err != nil {
		return nil, err
	}

	conn, err := net.Dial("tcp", hubConn.address)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	msg, err := message.ListMessagesRequest([]byte(localID.String()), []byte(remoteID.String()), chatID, secret)
	if err != nil {
		return nil, err
	}
	//Send share chatroom request
	_, err = conn.Write(msg)
	if err != nil {
		return nil, err
	}
	resp := make([]byte, 100)
	_, err = conn.Read(resp)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
