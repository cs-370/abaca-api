//The storage package holds subpackages for implementors of repositories.
//Implementors should be storage devices, such as a flat file, DB, or other medium.
package storage
