package knownerrors

const (
	UnauthorizedError   = "you are unauthorized. please sign in"
	InvalidRequestError = "bad request"
	BadGatewayError = "hub communication failed"
	InternalServerError = "internal server error"
)
