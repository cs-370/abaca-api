package security

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKeyPair(t *testing.T) {
	key, err := NewKey()
	if !assert.Nil(t, err) {
		return
	}
	msg := []byte("Test Msg")
	cipher, err := EncryptWithPublicKey(&key.PublicKey, msg)
	if !assert.Nil(t, err) {
		return
	}
	assert.NotEqual(t, msg, cipher)

	decoded, err := DecryptWithPrivateKey(key, cipher)
	if !assert.Nil(t, err) {
		return
	}
	assert.Equal(t, msg, decoded)
}
