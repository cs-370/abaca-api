package security

import (
	"crypto/rand"

	"golang.org/x/crypto/sha3"
)

//Generates a 16 byte random salt
func GenerateSalt() ([]byte, error) {
	var b = make([]byte, 16)
	var _, err = rand.Read(b)
	return b, err
}

func PasswordHash(username string, password string, salt []byte) ([]byte, error) {
	hash := sha3.New512()
	_, err := hash.Write(append([]byte(username+password), salt...))
	return hash.Sum(nil), err
}
