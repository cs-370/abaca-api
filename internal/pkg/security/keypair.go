package security

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"io/ioutil"
)

func NewKey() (*rsa.PrivateKey, error) {
	return rsa.GenerateKey(rand.Reader, 2048)
}

//Encrypts using Public Key.
// Can only be used with very short messages
func EncryptWithPublicKey(key *rsa.PublicKey, msg []byte) ([]byte, error) {
	return rsa.EncryptOAEP(sha256.New(), rand.Reader, key, msg, []byte("Abaca"))
}

func DecryptWithPrivateKey(key *rsa.PrivateKey, cipher []byte) ([]byte, error) {
	return rsa.DecryptOAEP(sha256.New(), rand.Reader, key, cipher, []byte("Abaca"))
}

func SaveKeyPair(filename string, key *rsa.PrivateKey) error {
	raw := x509.MarshalPKCS1PrivateKey(key)
	return ioutil.WriteFile(filename, raw, 0644)

}

func LoadKeyPair(filename string) (*rsa.PrivateKey, error) {
	raw, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return x509.ParsePKCS1PrivateKey(raw)
}
