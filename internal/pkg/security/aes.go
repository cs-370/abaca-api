package security

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
)

func Encrypt(key []byte, plaintext []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	var gcm cipher.AEAD
	if err == nil {
		gcm, err = cipher.NewGCM(block)
	}
	var nonce []byte
	if err == nil {
		nonce = make([]byte, gcm.NonceSize())
		_, err = io.ReadFull(rand.Reader, nonce)
	}
	if err == nil {
		ciphertext := gcm.Seal(nonce, nonce, plaintext, nil)
		return ciphertext, nil
	}
	return nil, err
}

func Decrypt(key []byte, ciphertext []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	var gcm cipher.AEAD
	if err == nil {
		gcm, err = cipher.NewGCM(block)
	}
	if err == nil {
		nonceSize := gcm.NonceSize()
		nonce, text := ciphertext[:nonceSize], ciphertext[nonceSize:]
		plaintext, err := gcm.Open(nil, nonce, text, nil)
		return plaintext, err
	}
	return nil, err
}
