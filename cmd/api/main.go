package main

import (
	"crypto/rsa"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/cs-370/abaca-api.git/internal/api"
	"gitlab.com/cs-370/abaca-api.git/internal/api/config"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/creating"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/deleting"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/linking"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/listing"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/storage/postgres"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/syncing"
	"gitlab.com/cs-370/abaca-api.git/internal/api/dal/updating"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers/auth"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers/chatroom"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers/hub"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers/message"
	"gitlab.com/cs-370/abaca-api.git/internal/api/routers/user"
	"gitlab.com/cs-370/abaca-api.git/internal/pkg/security"
)

func main() {

	//READ Config
	err := config.Init()
	if err != nil {
		fmt.Println("Error reading config:", err)
	}

	db, err := InitializeDB()
	if err != nil {
		fmt.Println("Couldn't open db:\n", err)
		return
	}

	//Get Keys used to verify server identity
	serverKey, err := InitializeServerKeys()
	if err != nil {
		fmt.Print("Could not generate or load server private key: ", err)
		return
	}

	//Initialize DAL
	Updater := updating.NewService(db)
	Deleter := deleting.NewService(db)
	//ID for this server
	serverID, err := db.GetServerID()
	if err != nil {
		fmt.Print("Could not retrieve current server ID: ", serverID)
		return
	}
	//Services that rely on having the syncing service available
	Sync := syncing.NewService(serverKey, serverID, db)
	Creator := creating.NewService(db, Sync)
	Lister := listing.NewService(db, Sync)
	Linker := linking.NewService(db, Sync, Lister)
	//Initialize hubs
	hubs, err := Lister.GetHubs()
	if err == nil {
		Sync.UpdateHubList(hubs)
	}

	//Initialize Routers
	router := InitializeServerEndpoints([]routers.EndpointSet{
		auth.NewAuthRouter(Linker, Lister),
		hub.NewHubRouter(Linker, Sync),
		user.NewUserRouter(Creator, Lister, Updater, Deleter),
		chatroom.NewChatroomRouter(Creator, Lister, Updater, Deleter),
		message.NewMessageRouter(Creator, Lister, Updater, Deleter),
	})
	//CORS
	router.Use(mux.CORSMethodMiddleware(router))
	//Initialize Server
	port := config.Port()
	fmt.Println("Listening on 0.0.0.0:", port)
	err = http.ListenAndServe(fmt.Sprint(":", port), api.NewServer(router))
	fmt.Println(err)

}

func InitializeDB() (postgres.Connection, error) {
	connStr := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=%s",
		config.DBUser(), config.DBPassword(),
		config.DBServer(), config.DBName(),
		config.DBsslMode(),
	)
	return postgres.NewConnection(connStr)
}

func InitializeServerEndpoints(routerList []routers.EndpointSet) *mux.Router {
	serverMux := mux.NewRouter()
	routers.MapEndpointsToServer(routerList, serverMux)

	serverMux.Use(mux.CORSMethodMiddleware(serverMux))

	return serverMux
}

func InitializeServerKeys() (*rsa.PrivateKey, error) {
	//Attempt to load an existing private key
	key, err := security.LoadKeyPair("abaca.pem")
	if err != nil {
		//Create a new key if load failed
		key, err = security.NewKey()
		if err == nil {
			err = security.SaveKeyPair("abaca.pem", key)
			if err != nil {
				return key, err
			}
		}
	}
	if key != nil {
		err = key.Validate()
	}
	return key, err
}
