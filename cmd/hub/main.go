package main

import (
	"fmt"

	"gitlab.com/cs-370/abaca-api.git/internal/hub"
)

func main() {
	server := hub.Hub{}
	fmt.Print(server.Serve())
}
