# Abaca Chat Server API
Configuration File: abaca.yaml
Default Parameters(present if omitted):
```yaml
abaca:
  #API HTTP Listen Port
  port: 3000
# DB Config
db:
  name: postgres
  host: 127.0.0.1
  user: postgres
  password: postgres
  sslmode: disable

```
## Docker
Run these commands from the project root directory

## Build the image
```
docker build -f ./build/Dockerfile -t registry.gitlab.com/cs-370/abaca-api/abaca:latest .
```


### Local run
Run the api on your machine:
```
go run cmd/api/main.go
```